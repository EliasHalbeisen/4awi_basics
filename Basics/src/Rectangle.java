
public class Rectangle {
	private int a, b;
	private int Area;
	private int Range;
	
	public Rectangle(int a, int b) {
		this.a = a;
		this.b = b;
	}
	
	public void sayHello() {
		System.out.println("Ich bin " + this.a + " breit und " + this.b + " lang");
	}
	
	public double getArea() {
		Area = a * b;
		return Area;
	
	}
	
	public double getRange() {
		Range = 2 * a + 2 * b;
		return Range;
	}
	
	
	
	

}
