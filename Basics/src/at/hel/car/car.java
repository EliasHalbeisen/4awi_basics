package at.hel.car;

public class car {
	
	private String color;
	private double basicPrice;
	private double maxSpeed;
	private double basicUsage;
	private producer producer;
	private double km;
	
	
	public double getKm() {
		return km;
	}
	public void setKm(double km) {
		this.km = km;
	}
	public producer getProducer() {
		return producer;
	}
	public void setProducer(producer producer) {
		this.producer = producer;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getBasicPrice() {
		return basicPrice;
	}
	public void setBasicPrice(double basicPrice) {
		this.basicPrice = basicPrice;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public double getBasicUsage() {
		return basicUsage;
	}
	public void setBasicUsage(double basicUsage) {
		this.basicUsage = basicUsage;
	}
	public car(String color, double basicPrice, double maxSpeed, double basicUsage, producer producer, double km) {
		super();
		this.color = color;
		this.basicPrice = basicPrice;
		this.maxSpeed = maxSpeed;
		this.basicUsage = basicUsage;
		this.producer = producer;
		this.km = km;
	}
	
	
	public double getUsage() {
		
		if (km > 50000) {
			basicUsage = (basicUsage * 1.098);
			
		}
		return basicUsage;
		
	}
		
		
		
	
	public double getPrice() {
		
		return this.basicPrice;
		
		
		
	}
	

}
