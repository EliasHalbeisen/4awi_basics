package at.hel.car;

public class engine {
	
	private double horsePower;
	private String egineType;
	public double getHorsePower() {
		return horsePower;
	}
	public void setHorsePower(double horsePower) {
		this.horsePower = horsePower;
	}
	public String getEgineType() {
		return egineType;
	}
	public void setEgineType(String egineType) {
		this.egineType = egineType;
	}
	public engine(double horsePower, String egineType) {
		super();
		this.horsePower = horsePower;
		this.egineType = egineType;
	}
	
	

}
