package at.hel.car;

public class producer {

	private String name;
	private String country;
	private double discount;
	
	
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public producer(String name, String country, double discount) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount;
	}
	
}
