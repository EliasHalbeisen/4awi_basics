package remote;

public class Battery {

	private int chargingStatus;
	public boolean hasPower;
	private String serialNumber;
	
	public Battery(int chargingStatus) {
		super();
		this.chargingStatus = chargingStatus;
	}

	public int getChargingStatus() {
		return chargingStatus;
	}

	public void setChargingStatus(int chargingStatus) {
		chargingStatus = chargingStatus;
	}
	
	public boolean hasPower() {
		if(chargingStatus > 50)
		{
			return true;
		}
		else {
			return false;
		}
	}
	

	
	
	
}
