package remote;

public class remote {
	
	private boolean isON = false;
	private boolean hasPower;
	private Battery battery;
	
	
	
	
	public remote(Battery battery) {
		super();
		this.battery = battery;
	}
	public Battery getBattery() {
		return battery;
	}
	public void setBattery(Battery battery) {
		this.battery = battery;
	}
	public void turnOn() {
		this.isON = true;
		System.out.println("I am turned on");
	}
	public void turnOff() {
		this.isON = false;
		System.out.println("I am turned off");
	}
	
	public boolean isON() {
		return this.isON;
	}
	
	
	

	
}
